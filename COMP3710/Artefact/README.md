README
---------------
- In Artefact, there are two files which are contains my analysis data, code and results. 
- Also, in the root directory, I have done much work in RVMType.java, RVMClass.java, RVMArray.java and MemoryManager.java to collect the information I need in my analysis.
- The analysis code is writen in python and the .csv file is my analysis data.
- These three file: RVMType.java, RVMClass.java, RVMArray.java are in rvm/src/org/jikesrvm/classloader/.
- And MemoryManager.java is in rvm/src/org/jikesrvm/mm/mminterface/.
- In these four java files, I have commentted where I added my code.
- Xalan's data for objects is too large to upload, so I upload the result of its analysis.