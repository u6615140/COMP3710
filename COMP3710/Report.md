Report
==================
Analysis of the use of immutable class state in Java workloads, and information-theoretic encoding of class state into limited bits in JikesRVM
------------------
Yi Kou

Acknowledgement
------------------
This is the report for COMP3710 in Australian National University enrolled by Yi Kou in semester 1, 2018. 
I would like to give my sincere thanks to my supervisor, Steve. His invaluable advice helped me a lot during 
this semester. Also, I would like to give my gratitude to examiner, Tony and course convener, Peter. Without 
their help, I could not have a satisfied result in this course. And I want to thank all members in MMTK group,
They gave me lots of good ideas to support my project. Last but not least, I would like to appreciate ANU and 
my home school, UCAS for providing me a chance to enjoy this half year in Canberra.

Abstract
------------------
In the Java garbage collection system, the TIB (Type Information Block) which incorporates information of each 
type for objects is very important for processing and identifying objects' reference fields. Also, TIBs need a 
large scale of memory to implement. There are many implementation methods for the TIB's allocator. By the 
inspiration of <a href="http://users.cecs.anu.edu.au/~steveb/downloads/pdf/scan-ismm-2011.pdf">A Comprehensive Evaluation 
of Object Scanning Techniques</a>, <i>"By selectively aligning the TIB (vtable) of each class, 
we effectively encode metadata into the header field that stores the TIB pointer"</i>, we desided to do a project on bit-encoding implementation. In this paper, Robin chose
to select 3 bits for encoding, so there exists 8 situations for this alignment to implement. However, in my project,
I selected various immutable type information to measure, for example the instance size, the judgement of array type 
and the alignment. This report will show the method I used to collect data, summarize data and analysis. By doing this 
work, I identify optimal bit-encoding of such variables which will be used to guide the implementation of an allocator 
for type information blocks (TIBs) with particular alignments, thereby encoding this type-specific state into the 
addresses of the TIBs.   

Introduction
------------------
The three bits encoding in Robin's research, which represent 8 situations of reference fields within objects are evaluated
by running benchmarks on MMTk. We can find out that the frequency of rank 1-8 has covered most of objects. So he put these
8 most common reference field counts into the 3-bits encoding.
<table>
    <tr>
        <td align="center"><img src="Reference_field_count.png" alt="Reference Field Counts"></td>
    </tr>
    <tr>
        <td align="center"><b>Figure 1. </b>Cumulative frequency distribution curves for reference field counts. Each graph 
            plots cumulative percentage of all objects (y-axis) covered by the N most common reference field counts (x-axis)</td>
    </tr>
</table> 

For me, I need to generalize the bit-encoding and try to find out the most important one(s) which could be encoded
into the addresses of TIBs. First of all, I worked out some probable immutable type information in class RVMClass 
and class RVMArray. And then for each TIB, I print out messages of them based on those immutable type information 
to find out the immutable states in them. Also, I need to number all TIBs which have been built either in build-time 
or run-time. By summarizing that data, I could quickly analysis whether the state is immutable or not as well the
 frequency and the important of it for tib-encoding.

Due to the abundant objects in each TIB, discovering those immutable states on each objects is also a vital work in 
this project. Because of the specific properties in TIB class and the one-to-one relationship between TIB and objects' 
type, I could only marked each object when it was created based on their own type. Then, instrument the virtual machine
to measure the frequency with which the state is used. 

The analysis depends on the result I got when run the code on different benchmarks in Dacapo. <i><a href="http://dacapobench.org">
Dacapo benchmark suite</a> is intended as a tool for Java benchmarking by the programming language, memory 
management and computer architecture communities. It consists of a set of open source, real world applications 
with non-trivial memory loads.</i>

After the analysing that data, The measurement shows that those type information states which I will show in results 
coulb be encoded into allocator to determine TIBs' addresses.


Methodology
-------------------
- Immutable Type Information Determination
For the purpose of picking up probable immutable states, I went through the RVMClass class and RVMArray class to decide
which variables might be the immutable one in Java workload. As a result, I selected these immutable variables in those 
various type information.

<table>
    <tr>
        <th width=30%, bgcolor=blue >immutable type information</th>
        <th width=70%, bgcolor=blue >annotation</th>
    </tr>
    <tr>
        <td>is array</td>
        <td>Describe whether the type is a class or an array</td>
    </tr>
        <td>instance size</td>
        <td>Tell instance size defined in this type</td>
    <tr>
        <td>modifier</td>
        <td>A bit-data shows that the type is final? public? statics? or have other else modifiers</td>
    </tr>
    <tr>
        <td>finalization</td>
        <td>A kind of mark which is useful in garbage collection</td>
    </tr>
    <tr>
        <td>log element size</td>
        <td>The log of the element size for array type or class type</td>
    </tr>
    <tr>
        <td>alignment</td>
        <td>The desired alignment for instances of this type</td>
    </tr>
    <tr>
        <td>dimension</td>
        <td>Dimension message for array type</td>
    </tr>
</table>

- Collecting Data
Since collecting data about TIB from Jikes RVM mostly determined by finding a TIB when it was being created during build
time and run time.In Jikes RVM, there is a class for RVMType. Also a method named newTIB() for initializing new TIB in 
memory manager. So I numbered and marked each TIB after it had been initiated by the newTIB() method which is called in 
the RVMType class. Therefore, every TIB has its own number, which I named tib_num to distinguish them form their own 
states I collected. In order to print out their properties which I had selected as immutable type information, I wrote 
lines in class RVMClass and class RVMArray with the help of 'VMWriteln' to show these quantities of TIB characristics
by running most of benchmarks in Dacapo benchmark suite. I have ran "fop", "xalan", "antlr", "bloat", "eclipse", "jython"
and "pmd" benchmarks. Then I collected a quantity of data to analysis.
The following is code which I implement in RVMClass to print out message for collecting.
````java
if(isClassType()){
      VM.sysWriteln(count_tib + "," +
              (isArrayType() ? "1" : "0") + "," +
              instanceSize + "," +
              + getOriginalModifiers() +  "," +
              (!hasFinalizer ? "0" : "1") + "," +
              "," +
              alignment +
              ","
      );
    }
````
As the above code is to measure the immutable type information for each TIB as well as for each type, I also need to discover 
the frequency with which the state is used for each objects. Because in above experiment I find that the number of array 
type is much less than the number of class type, so I have to measure on objects again. Steve told me that maybe the number
of objects in those array type is much bigger than in class type. The main idea is not just analysis the immutable states 
in TIB or type, measurements on objects is also a vital task. As same as collecting data for TIB, objects shoulb also be 
numbered and marked. I firstly tried to add a field 'count_number' in class TIB for recording which TIB it belongs to. 
However, TIB class could not successful built with modified fields. I was suggested to take a closer look into types, due to 
its one-to-one relationship with TIB. So I add a field into RVMType class and methods to set this field when newTIB are 
initialized by the allocator and get this field when objects are created in this specific type with the TIB which have 
marked before.I also mentioned that two methods in memory manager, 'allocateScalar' and 'allocateArray' are called to 
initialize objects in class type or in array type. By adding lines in those two methods like following, I finally print 
all the objects with this specific mark, tib_num.
````java
    int count = tib.getType().getTIBNum();
    VM.sysWriteln(count);
````

- Analysis and Measurement
I have wrote a script in Python to analyse and measure the data which I got from running such benchmarks. I considered that
the total number of TIB, the number of class type or array type, the probable log element size for array type, the possible 
alignment for class type, every kinds of modifier the class type TIB could have, the dimension of each array type and the 
instance size for each TIB. By importing seaborn, I draw barplot for the count number of TIB in each instance size and the 
total number of TIBs which has same modifier. Also, <i><a href="https://en.wikipedia.org/wiki/Cumulative_frequency_analysis">
Cumulative Frenquency Analysis</a> is the analysis of the frequency of occurrence of values of a phenomenon less than a 
reference value.</i> By using this analysis method, I summarized the frequency with which state is used and picked up the important 
immutable states and identify optimal bit-encoding of such variables, which will be used to guide the implementation of an 
allocator for TIB with particular alignments for encoding this type-specific state into the addresses of the TIBs.

Results
--------------------
After running "fop", "xalan", "antlr", "bloat", "eclipse", "jython" and "pmd" benchmarks in Dacapo benchmark suite, then summarize 
the data and ran the script to analyse. Finally, got the following result.
- For Is Array

Because the array type is much less than class type in the TIB analysis. So we have to do measurements on objects. I only ran "fop"
benchmark because of the huge number of data. Here is the Pie graph for isarray type and isclass type in fop benchmark.

<table>
    <tr>
        <td align="center"><img src="objects_array.png" alt="Pie Plot for Array Objects"></td>
    </tr>
    <tr>
        <td align="center"><b>Figure 2. </b>Pie Plot for different TIBs' Array Objects. It shows the proportion of array objects 
        in each TIBs.</td>
    </tr>
    <tr>
        <td align="center"><img src="objects_class.png" alt="Pie Plot for Class Objects"></td>
    </tr>
    <tr>
        <td align="center"><b>Figure 3. </b>Pie Plot for different TIBs' Class Objects. It shows the proportion of class objects 
        in each TIBs.</td>
    </tr>
    <tr>
        <td align="center"><img src="objects.png" alt="Pie Plot for Total Objects"></td>
    </tr>
    <tr>
        <td align="center"><b>Figure 4. </b>Pie Plot for different TIBs' Objects. It shows the proportion of objects 
        in each TIBs.</td>
    </tr>
</table>

It indicates that the top 8 TIB which have the most objects covered a lot in total array type TIBs' objects. But in class type and in total
objects, we could not summarize limited number(more than 5 bits = 32) of states to represent whole objects. So just in array type we could 
encode 3 bits for them to implement the allocator.
    
- For Instance Size

<table>
    <tr>
        <td align="center"><img src="inssize_TIB_benchmarks.png" alt="Kinds of Instance Size Count for TIB from Different Benchmarks"></td>
    </tr>
    <tr>
        <td align="center"><b>Figure 5. </b>Bar Plot for different kinds of instance size count number from different benchmarks. Seven 
            benchmarks' graph in one figure. Each graph plots show the number of TIBs with each instance size (y-axis) covered by the N 
            most common instance size (x-axis)</td>
    </tr>
    <tr>
        <td align="center"><img src="CFD_inssize_TIB.png" alt="Cumulative frequency distribution curves for Instance Size Count for TIB"></td>
    </tr>
    <tr>
        <td align="center"><b>Figure 6. </b>Cumulative frequency distribution curves for Instance Size. Each graph 
            plots cumulative count of each TIB's (type's) instance size (y-axis) covered by the N most common Instance Size (x-axis)</td>
    </tr>
</table>

From the above two graphs, it is obviously that the most common instance size for TIB are 8.0(4716), 16.0(971), 44.0(839), 20.0(668),
12.0(601), 24.0(301), 32.0(234), 28.0(201), 40.0(184) and 56.0(133). The total number of TIBs with these ten most frequent instance 
size cover 93.5% (8942/9462) within whole kinds of instance size for total TIBs. So the above ten kinds of instance size should be 
taken into consideration. If we add them into the tib-encoding allocator, it will save so much time and space to scan the objects, since
we can already find them because of their addresses.
    
- For Modifiers

<table>
    <tr>
        <td align="center"><img src="modifier_TIB.png" alt="Kinds of Modifiers for TIB from Different Benchmarks"></td>
    </tr>
    <tr>
        <td align="center"><b>Figure 7. </b>Bar Plot for different kinds of modifiers from different benchmarks. Seven 
            benchmarks' graph in one figure. Each graph plots show the number of TIBs with each instance size (y-axis) covered by different kinds
            of modifiers (x-axis)</td>
    </tr>
    <tr>
        <td align="center"><img src="modifier_TIB_CFD.png" alt="Cumulative frequency distribution curves for Modifiers for TIB"></td>
    </tr>
    <tr>
        <td align="center"><b>Figure 8. </b>Cumulative frequency distribution curves for Modifiers. Each graph 
            plots cumulative count of each TIB's (type's) modifiers (y-axis) covered by the different kinds of modifiers (x-axis)</td>
    </tr>
</table> 

By summarizing the above two figures, we can know that the most six common modifiers for type are 17.0(3653), 1.0(2464), 9.0(950), 1537.0(698), 0.0(693)
, 1025.0(423). The total number of TIBs which have these modifiers take 93.9%(8994/9462) proportion which means the six modifiers could cover 
most of TIBs when they are encoded into the allocator.
    
- For Has Finalizer

After running the seven benchmarks, I found that hasFinalizer always return 0, which means that TIBs usually don't have finalizer. In the total 9462
TIBs, there are only 3 TIBs have finalizer. It seems that not having a finalizer could be a tib in the tib-encoding allocator. 

- For Log Element Size and Dimension

Because these two type information states are specific for array, so I only measure them in array type. It is summarized that in array type, dimension
is always 1.0 and log element size is always 2.0. So these two type information states could also be encoded into the particular alignments for TIBs. 

- For Alignment

By Summarizing alignment part, I found out that 4.0(8350) and 0.0(728) takes 98.2% of total TIBs. So there could be a bit to encode for alignment which 
determines 4.0 or 0.0 alignment for TIBs.

To Be Improved
----------------------
- It will be better if I ran more benchmarks to analyse. Since I spent too much time on understanding the java code and learning how to use python to
analyse data, I had no time to do more work on this project at the end of the semester. Also, choosing benchmarks besides Dacapo might not be a bad 
decision in the future experiment.
- I tried my best to collect the tyoe information states for TIBs as well as for objects. However, the tib-encoding still needs much more than that.
Like the frequency of methods have been called and searching out more immutable type information states are also matters we should take a closer 
look into.


Reference
---------------------
[1]Robin Garner, Stephen M Blackburn, Daniel Frampton. A Comprehensive Evaluation of Object Scanning Techniques.
    In <i>ISMM '11 Proceedings of the international symposium on Memory management</i>, Pages 33-42, June 2011. 


